**GITLAB**

- GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager. 

- GitLab features wiki, issue-tracking and continuous integration/deployment pipeline features.

- It helps teams come with a efficient code by remotely combining all the bits of code into a single code.

[Gitlab Interface](https://about.gitlab.com/images/blogimages/redesigning-gitlabs-navigation/final.png)

