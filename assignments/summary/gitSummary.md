**Git**

Git is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people. 

**Git Workflow**

Before we start working with Git commands, it is necessary that you understand what it represents.

**Repository**

A repository(repo) is nothing but a collection of source code.

**Fundamental elements in the Git Workflow**

1. Working Directory

2. Staging Area

3. Local Repository 

4. Remote Repository

[Git Workflow!](https://hackernoon.com/hn-images/1*9qX9F9MGsWKfcmgTOR9BPw.png).

